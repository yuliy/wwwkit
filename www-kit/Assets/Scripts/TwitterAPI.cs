﻿using UnityEngine;

using System;

using System.Collections.Generic;
using System.IO;

using WWWKit;
using WWWKit.Authenticators;

/// <summary>
/// Types of Commands allowed in Upload Media Chunked Calls
/// </summary>
public enum TwitterUploadCommand {
    INIT,
    APPEND,
    FINALIZE,
    UNCHUNKED
}

public class TwitterAPI : MonoBehaviour {
	const int CHUNKSIZE = 512000; // 512kb
	const string TWITTER_BASE_URL = "https://api.twitter.com/";
	const string UPLOAD_API_BASE_URL = "https://upload.twitter.com/";
	
	public string oauthConsumerKey = "JOE024N9poW2CYqr0bXmJh6ml";
	public string oauthConsumerSecret = "lOrmy6jKOMVMiWq0LinUM1uTMp6vsECnRYDWj4AsqEut4ODURH";
	
	public string oauthToken = "3240353534-cJA5S6Rv1vPZRPhIIQ8ww4uU0k5U3QppOvJjGwU";
	public string oauthTokenSecret = "DprLqay35e2gcEbb3CRZPEhnuRyRoPKBzvuNJl7jAhJ2P";
// 	public string oauthToken = "751234076-TSDW8bM0vSnaP1W2QKulnmikHmsEAAFzBuDOhGlr";
// 	public string oauthTokenSecret = "z09QL84NI0GG60kA9otxFgmXn6OyKFJSVzTMett2KQnlk";
//     public string oauthToken = "";
//     public string oauthTokenSecret = "";
	
	private static TwitterAPI _instance;
	public static TwitterAPI Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new GameObject("TwitterAPI").AddComponent<TwitterAPI>();
			}
			return _instance;
		}
	}
    /// <summary>
    /// Split a file represented by a byte Array and return a List of byte Arrays.
    /// </summary>
	private List<byte[]> ChunkFile(byte[] file) {
		int fileByteLength = file.Length;
		
		List<byte[]> chunks = new List<byte[]>();
		int chunkQty = (int)Mathf.Ceil((float)fileByteLength / (float)TwitterAPI.CHUNKSIZE);
		for(int i = 0; i < chunkQty - 1; i++) {
			var f = new byte[TwitterAPI.CHUNKSIZE];
			Buffer.BlockCopy(file, TwitterAPI.CHUNKSIZE * i, f, 0, TwitterAPI.CHUNKSIZE);
			chunks.Add(f);
		}
		int lastChunk = chunkQty - 1;
		int lastSize = fileByteLength - TwitterAPI.CHUNKSIZE * lastChunk;
		var lf = new byte[lastSize];
		Buffer.BlockCopy(file, TwitterAPI.CHUNKSIZE * lastChunk, lf, 0, lastSize);
		chunks.Add(lf);
		
		return chunks;
	}
    
    /// <summary>
    /// Build a WWWRequest that represents an Upload Media APPEND command for Chunked Media Upload.
    /// </summary>
    private WWWRequest BuildAppendRequest(byte[] chunk, string mediaId, int index) {
        WWWRequest request = new WWWRequest("1.1/media/upload.json", HttpMethod.POST);
        request.AlwaysMultipart = true; // must be set so that the Request will be a multipart/form-data POST
        request.AddBinaryData("media", chunk);
        request.AddParameter("command", TwitterUploadCommand.APPEND.ToString());
        request.AddParameter("media_id", mediaId);
        request.AddParameter("segment_index", index.ToString());
        return request;
    }
    
    /// <summary>
    /// Utility function to default handle a WWWResponse when received from a WWWClient.Execute call.
    /// </summary>
    public void DefaultOnComplete(WWWResponse response) {
        if (response.Success()) {
            Debug.Log(response.Content());
			Debug.Log(response.StatusCode());
        } else {
            Debug.Log(response.Errors());
            Debug.Log(response.StatusCode());
        }
    }
	
    /// <summary>
    /// Request an OAuth Request Token to initialize the OAuth Authentication flow.
    /// </summary>
	public void RequestToken(Action<TwitterOAuthTokenResponse> onComplete = null) {
        WWWClient client = new WWWClient(TwitterAPI.TWITTER_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForRequestToken(oauthConsumerKey, oauthConsumerSecret);
        WWWRequest request = new WWWRequest("oauth/request_token", HttpMethod.POST);
        
        Action<TwitterOAuthTokenResponse> defaultOnComplete = response => {
            if (response.Success()) {
                // pull the oauth_token and oauth_token_secret from the response
                var token = response.ContentAtKey("oauth_token");
                var tokenSecret = response.ContentAtKey("oauth_token_secret");
                
                this.oauthToken = token;
				this.oauthTokenSecret = tokenSecret;
                
                // Create the url of the format https://api.twitter.com/oauth/authorize?oauth_token=XXXX
                // this URL will automatically be opened in the Browser so that the user can accept the
                // authorization.
				Application.OpenURL(TwitterAPI.TWITTER_BASE_URL + "oauth/authorize?oauth_token=" + this.oauthToken);
            } else {
                Debug.Log(response.Errors());
            }
		};
        
        StartCoroutine(
            client.Execute<TwitterOAuthTokenResponse>(
                request,
                onComplete ?? defaultOnComplete
            )
        );
	}
	
    /// <summary>
    /// Request an OAuth Access Token given a verifier pin.
    /// </summary>
	public void RequestAccessToken(string verifier, Action<TwitterOAuthTokenResponse> onComplete = null) {
        WWWClient client = new WWWClient(TwitterAPI.TWITTER_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForAccessToken(
            oauthConsumerKey,
            oauthConsumerSecret,
            oauthToken,
            verifier
        );
        WWWRequest request = new WWWRequest("oauth/access_token", HttpMethod.POST);
        
        Action<TwitterOAuthTokenResponse> defaultOnComplete = response => {
            if (response.Success()) {
                // pull the oauth_token and oauth_token_secret from the response
                var token = response.ContentAtKey("oauth_token");
                var tokenSecret = response.ContentAtKey("oauth_token_secret");
                
                this.oauthToken = token;
				this.oauthTokenSecret = tokenSecret;
		
				this.Verify();
            } else {
                Debug.Log(response.Errors());
            }
        };
        
        StartCoroutine(
            client.Execute(
                request,
                onComplete ?? defaultOnComplete
            )  
        );
	}
	
    /// <summary>
    /// Verify our Twitter credentials.
    /// </summary>
	public void Verify(Action<TwitterRestResponse> onComplete = null) {
        WWWClient client = new WWWClient(TwitterAPI.TWITTER_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForProtectedResource(
            oauthConsumerKey,
            oauthConsumerSecret,
            oauthToken,
            oauthTokenSecret
        );
        WWWRequest request = new WWWRequest("1.1/account/verify_credentials.json", HttpMethod.GET);
        StartCoroutine(
            client.Execute(
                request,
                onComplete ?? DefaultOnComplete
            )
        );
	}
    
    /// <summary>
    /// Upload file to Twitter via Twitter's Upload Media API.  This version of the function will load a file path into a byte Array and call UploadMedia(byte[], Action)
    /// </summary>
    public void UploadMedia(string mediaPath, Action<TwitterUploadMediaResponse> onComplete = null)
    {
        byte[] file = File.ReadAllBytes(mediaPath);
        this.UploadMedia(file, onComplete);
    }
    
    /// <summary>
    /// Upload file to Twitter via Twitter's Upload Media API.
    /// </summary>
    public void UploadMedia(byte[] file, Action<TwitterUploadMediaResponse> onComplete = null)
    {
        WWWClient client = new WWWClient(TwitterAPI.UPLOAD_API_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForProtectedResource(
            oauthConsumerKey,
            oauthConsumerSecret,
            oauthToken,
            oauthTokenSecret
        );
        
        WWWRequest request = new WWWRequest("1.1/media/upload.json", HttpMethod.POST);
        request.AlwaysMultipart = true; // must be set so that the Request will be a multipart/form-data POST
        request.AddBinaryData("media", file);
        
        StartCoroutine(
            client.Execute(
                request,
                onComplete ?? DefaultOnComplete
            )
        );
    }
    
    /// <summary>
    /// Default function to build the Action<TwitterUploadMediaResponse> which will control the flow of the Upload process.
    /// </summary>
    public Action<TwitterUploadMediaResponse> DefaultBuildUploadFlow(byte[] file, Action<TwitterUploadMediaResponse> onFinalize = null) {
        Action<TwitterUploadMediaResponse> onInit = initResponse => {
            if (initResponse.Success()) {
                print(initResponse.Content());
                print(initResponse.StatusCode());
                
                // Pull the media_id_string off of the response
                string mediaId = (string) initResponse.ContainsAtKey("media_id_string");
                
                Action<TwitterUploadMediaResponse> onAppendComplete = appendResponse => {
                    if (appendResponse.Success()) {
                        print(appendResponse.Content());
    					print(appendResponse.StatusCode());
                            
                        UploadMediaChunked_Finalize(mediaId, onFinalize);
                    } else {
                        print(appendResponse.Errors());
                    }
                };
                
                UploadMediaChunked_Append(file, mediaId, null, onAppendComplete);
            } else {
                Debug.Log(initResponse.Errors());
            }
        };
        
        return onInit;
    }
    
    /// <summary>
    /// Wrapper for UploadMediaChunked(byte[], Action<TwitterUploadMediaResponse> which allows a string parameter which is a file path.  This
    /// file will be read into a byte Array.
    /// </summary>
    /// <param name="mediaPath"></param>
    /// <param name="onFinalize"></param>
    public void UploadMediaChunked(string mediaPath, Action<TwitterUploadMediaResponse> onFinalize = null)
    {
        byte[] file = File.ReadAllBytes(mediaPath);
        UploadMediaChunked(file, onFinalize);
    }
    
    /// <summary>
    /// Entry point for Uploading a Media file to Twitter in a chunked format.  Accepts an optional parameter which will be the callback
    /// when the entire process is completed.  This function will use DefaultBuildUploadFlow to generate the flow and pass in this
    /// onFinalize Action to complete the process.
    /// </summary>
    /// <param name="file"></param>
    /// <param name="onFinalize"></param>
    public void UploadMediaChunked(byte[] file, Action<TwitterUploadMediaResponse> onFinalize = null)
    {
        UploadMediaChunked_Init(
            file,
            DefaultBuildUploadFlow(file, onFinalize)
        );
    }
    
    /// <summary>
    /// Wrapper for UploadMediaChunked(byte[], Func<byte[], Action<TwitterUploadMediaResponse>>) which allows a string parameter which
    /// will be read into a byte Array.
    /// </summary>
    public void UploadMediaChunked(string mediaPath, Func<byte[], Action<TwitterUploadMediaResponse>, Action<TwitterUploadMediaResponse>> buildUploadFlow = null)
    {
        byte[] file = File.ReadAllBytes(mediaPath);
        UploadMediaChunked(file, buildUploadFlow);
    }
    
    /// <summary>
    /// Entry point for Uploading a Media file to Twitter in a chunked format.  Accepts an optional parameter which is a Func which builds the
    /// Action<TwitterUploadMediaResponse> which controls the flow of the Upload process.
    /// </summary>
    public void UploadMediaChunked(byte[] file, Func<byte[], Action<TwitterUploadMediaResponse>, Action<TwitterUploadMediaResponse>> buildUploadFlow = null)
    {
        // lol...
        UploadMediaChunked_Init(
            file, 
            (buildUploadFlow ?? DefaultBuildUploadFlow)(file, null)
        );
    }
    
    /// <summary>
    /// Create and execute the first part of Uploading a chunked Media file to Twitter (the INIT command).
    /// </summary>
    public void UploadMediaChunked_Init(byte[] file, Action<TwitterUploadMediaResponse> onInit = null)
    {
        WWWClient client = new WWWClient(TwitterAPI.UPLOAD_API_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForProtectedResource(
            oauthConsumerKey,
            oauthConsumerSecret,
            oauthToken,
            oauthTokenSecret
        );
        
        // Build the INIT request
        WWWRequest request = new WWWRequest("1.1/media/upload.json", HttpMethod.POST);
        request.AlwaysMultipart = true; // must be set so that the Request will be a multipart/form-data POST
        request.AddParameter("command", TwitterUploadCommand.INIT.ToString());
        request.AddParameter("media_type", "video/mp4");
        request.AddParameter("total_bytes", file.Length.ToString());
        
        // Build the default Action to execute after the INIT request
        Action<TwitterUploadMediaResponse> defaultOnInit = initResponse => {
            if (initResponse.Success()) {
                print(initResponse.Content());
                print(initResponse.StatusCode());
                
                // Pull the media_id_string off of the response
                string mediaId = (string) initResponse.ContainsAtKey("media_id_string");
                
                UploadMediaChunked_Append(file, mediaId);
            } else {
                Debug.Log(initResponse.Errors());
            }
        };
        
        StartCoroutine(
            client.Execute<TwitterUploadMediaResponse>(
                request,
                onInit ?? defaultOnInit
            )
        );
    }
    
    /// <summary>
    /// Create and execute the APPEND commands as part of Uploading a chunked Media file to Twitter.
    /// </summary>
    public void UploadMediaChunked_Append(byte[] file, string mediaId, Action<TwitterUploadMediaResponse> onAppend = null, Action<TwitterUploadMediaResponse> onAppendComplete = null)
    {
        // Chunk the File and build all the APPEND requests    
		var chunks = this.ChunkFile(file);
        List<WWWRequest> requests = new List<WWWRequest>();
        for(int i = 0; i < chunks.Count; i++) {
            requests.Add(this.BuildAppendRequest(chunks[i], mediaId, i));
        }
        
        WWWClient client = new WWWClient(TwitterAPI.UPLOAD_API_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForProtectedResource(
            oauthConsumerKey,
            oauthConsumerSecret,
            oauthToken,
            oauthTokenSecret
        );
        
        Action<TwitterUploadMediaResponse> defaultOnAppendComplete = finalizeResponse => {
            if (finalizeResponse.Success()) {
                print(finalizeResponse.Content());
				print(finalizeResponse.StatusCode());
                
                UploadMediaChunked_Finalize(mediaId);
            } else {
                print(finalizeResponse.Errors());
            }
        };
        
        StartCoroutine(
            client.Execute_Multi<TwitterUploadMediaResponse>(
                requests,
                onAppend ?? DefaultOnComplete,
                onAppendComplete ?? defaultOnAppendComplete
            )
        );
    }
    
    /// <summary>
    /// Create and execute the FINALIZE command which is the final part of Uploading a chunked Media file to Twitter.
    /// </summary>
    public void UploadMediaChunked_Finalize(string mediaId, Action<TwitterUploadMediaResponse> onFinalize = null)
    {
        // Create our Client and setup an OAuth1 Authenticator on it
        WWWClient client = new WWWClient(TwitterAPI.UPLOAD_API_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForProtectedResource(
            oauthConsumerKey,
            oauthConsumerSecret,
            oauthToken,
            oauthTokenSecret
        );
        
        // Build the FINALIZE request
		WWWRequest finalizeRequest = new WWWRequest("1.1/media/upload.json", HttpMethod.POST);
        finalizeRequest.AlwaysMultipart = true; // must be set so that the Request will be a multipart/form-data POST
        finalizeRequest.AddParameter("command", TwitterUploadCommand.FINALIZE.ToString());
        finalizeRequest.AddParameter("media_id", mediaId);
        
        // Use the WWWClient to execute the FINALIZE request
        StartCoroutine(
            client.Execute<TwitterUploadMediaResponse>(
                finalizeRequest,
                onFinalize ?? DefaultOnComplete
            )
        );
    }
	
    /// <summary>
    /// Post a Tweet with status.  Optionally provide a media_id_string to attach media to the Tweet.
    /// </summary>
	public void PostTweet(string status, string mediaId = null, Action<TwitterPostTweetResponse> onComplete = null) {
		WWWClient client = new WWWClient(TwitterAPI.TWITTER_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForProtectedResource(
            oauthConsumerKey,
            oauthConsumerSecret,
            oauthToken,
            oauthTokenSecret
        );
        
        WWWRequest request = new WWWRequest("1.1/statuses/update.json", HttpMethod.POST);
        request.AddParameter("status", status);
        
        // if mediaId is set then we need to add it to the Request parameters
        if (mediaId != null) {
            request.AddParameter("media_ids", mediaId);
        }
        
        StartCoroutine(
            client.Execute<TwitterPostTweetResponse>(
                request,
                onComplete ?? DefaultOnComplete
            )
        );
	}
    
    /// <summary>
    /// Search Tweets by a set of comma separated keywords.
    /// </summary>
    public void SearchTweets(string keywords, Action<TwitterRestResponse> onComplete = null) {
        WWWClient client = new WWWClient(TwitterAPI.TWITTER_BASE_URL);
        client.Authenticator = OAuth1Authenticator.ForProtectedResource(
            oauthConsumerKey,
            oauthConsumerSecret,
            oauthToken,
            oauthTokenSecret
        );
        
        WWWRequest request = new WWWRequest("1.1/search/tweets.json", HttpMethod.GET);
        request.AddParameter("q", keywords);
        request.AddParameter("count", "100");
        request.AddParameter("result_type", "popular");
        
        StartCoroutine(
            client.Execute<TwitterRestResponse>(
                request,
                onComplete ?? DefaultOnComplete
            )
        );
    }
}
