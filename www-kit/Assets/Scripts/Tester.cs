﻿using UnityEngine;

using System;
using System.Collections.Generic;

using Frapper;

public class Tester : MonoBehaviour {
	private string pinField = "";
    
    private void UploadPictureAndPostTweet(string mediaPath)
    {
        UploadPicture(
            mediaPath,
            HandleUploadResponse
        );
    }
    
    private void ConvertAndPostAviVideoTweet(string aviPath)
    {
        ConvertAvi(
            aviPath, 
            "Assets/output.mp4",
            convertStatus =>
            {
                if (convertStatus)
                {
                    UploadConvertedVideo(
                        "Assets/output.mp4",
                        HandleUploadResponse
                    );
                } else
                {
                    // error
                }
            }
        );
    }
    
    private void HandleUploadResponse(TwitterUploadMediaResponse finalizeResponse)
    {
        if (finalizeResponse.Success())
        {
            print(finalizeResponse.Content());
            print(finalizeResponse.StatusCode());
            
            string finalizedMediaId = finalizeResponse.GetMediaIdString();
            if (finalizedMediaId != null)
            {
                PostMediaTweet("status", finalizedMediaId,
                    HandleTweetResponse
                );
            } else {
                // error
            }
        } else
        {
            // error
            print(finalizeResponse.Errors());
        }
    }
    
    private void HandleTweetResponse(TwitterPostTweetResponse tweetResponse)
    {
        if (tweetResponse.Success())
        {
            string tweetId = tweetResponse.GetTweetIdStr();
            string screenName = tweetResponse.GetScreenName();
            
            Application.OpenURL("http://twitter.com/" + screenName + "/status/" + tweetId);
        } else
        {
            // error
            print(tweetResponse.Errors());
        }
    }
    
    private void ConvertAvi(string aviPath, string outputPath, Action<bool> onComplete)
    {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
		FFMPEG ffmpeg = new FFMPEG("Assets/bin/ffmpeg.exe");
#elif UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
		FFMPEG ffmpeg = new FFMPEG("Assets/bin/ffmpeg");
#else
		FFMPEG ffmpeg = null;
#endif

    	if (ffmpeg != null)
        {
    		// build the ffmpeg command
    		string parameters = ffmpeg.BuildVideoConvertParameters(aviPath, outputPath, true, FFMPEG.CODEC_H264, 64, 768);
    
    		ffmpeg.RunProcess(
    			parameters,
    			onComplete
            );
        }
    }
    
    private void UploadConvertedVideo(string mediaPath, Action<TwitterUploadMediaResponse> onComplete)
    {
        TwitterAPI.Instance.UploadMediaChunked(
            mediaPath,
            finalizeResponse =>
            {
                onComplete(finalizeResponse);
            }
        );
    }
    
    private void PostMediaTweet(string status, string mediaId, Action<TwitterPostTweetResponse> onComplete)
    {
        TwitterAPI.Instance.PostTweet(
            status,
            mediaId, 
            tweetResponse =>
            {
                onComplete(tweetResponse);
            }
        );
    }
    
    private void UploadPicture(string mediaPath, Action<TwitterUploadMediaResponse> onComplete)
    {
         TwitterAPI.Instance.UploadMedia(
             mediaPath,
             onComplete
         );
    }
    
	void OnGUI () {
		// Make a background box
		GUI.Box(new Rect(10,10,340,300), "Twitter API");
		
		pinField = GUI.TextField (new Rect (20, 120, 120, 20), pinField);
		
		if(GUI.Button(new Rect(20,80,120,20), "Request Token")) {
			TwitterAPI.Instance.RequestToken();
		}
		
		if(GUI.Button(new Rect(155,120,120,20), "Request Access")) {
			TwitterAPI.Instance.RequestAccessToken(pinField);
		}
		
		if(GUI.Button(new Rect(20,160,120,20), "Verify")) {
			TwitterAPI.Instance.Verify();
		}
		
		if(GUI.Button(new Rect(20,200,120,20), "Post Video")) {
    	   ConvertAndPostAviVideoTweet("Assets/UploadMedia/sonic.avi");
        }
        
        if(GUI.Button(new Rect(20,240,120,20), "Post Picture")) {
            UploadPictureAndPostTweet("Assets/UploadMedia/png_test.png");
        }
        
        if(GUI.Button(new Rect(20,280,120,20), "Search")) {
            TwitterAPI.Instance.SearchTweets("sonic");
        }
    }
}
