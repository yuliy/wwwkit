﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;

using MiniJSON;
using WWWKit;

public class TwitterOAuthTokenResponse : WWWResponse {
    private Dictionary<string, string> _ResponseContent = null;
    public Dictionary<string, string> ResponseContent
    {
        get
        {
            if (_ResponseContent == null)
            {
                _ResponseContent = QueryStringToDictionary();
            }
            
            return _ResponseContent;
        }
    }
    
    /// <summary>
    /// Overriding the default behaviour here so that we will also ensure that the Response Content includes the
    /// two keys "oauth_token" and "oauth_token_secret".  If they are not present then our request was not
    /// actually successfully (regardless of the status code)
    /// </summary>
    override public bool Success()
    {
        if (!base.Success())
        {
            return false;
        }
        
        return ResponseContent.ContainsKey("oauth_token") && ResponseContent.ContainsKey("oauth_token_secret");
    }
    
    /// <summary>
    /// Utility funciton to retrieve a specific key from the Response Content
    /// </summary>
    /// <param name="key"></param>
    public string ContentAtKey(string key)
    {
        if (ResponseContent.ContainsKey(key))
        {
            return ResponseContent[key];
        }
        
        return null;
    }
}

public class TwitterRestResponse : WWWResponse {
    private IDictionary _ResponseContent = null;
    public IDictionary ResponseContent {
        get
        {
            if (_ResponseContent == null)
            {
                _ResponseContent = (IDictionary) Json.Deserialize(Content());
            }
            
            return _ResponseContent;
        }
    }
    
    private IDictionary _ErrorsContent = null;
    public IDictionary ErrorsContent {
        get
        {
            if (_ErrorsContent == null)
            {
                _ErrorsContent = (IDictionary) Json.Deserialize(Errors());
            }
            
            return _ErrorsContent;
        }
    }
    
    protected HttpStatusCode SuccessStatus = HttpStatusCode.OK;
    
    /// <summary>
    /// Overriding the default functionality here to ensure that the Response Status Code is the same
    /// as our defined SuccessStatus.
    /// </summary>
    override public bool Success()
    {
        if (!base.Success())
        {
            return false;
        }
        
        return StatusCode() == SuccessStatus;
    }
    
    /// <summary>
    /// Overriding the default functionality here because occassionally for Twitter REST responses 
    /// the Status Code could be an acceptable one from the Unity perspective and errors will actually
    /// be present in the body content of the repsonse.
    /// </summary>
    override public string Errors()
    {
        if (base.Errors() == null)
        {
            return Content();
        }
        
        return base.Errors();
    }
    
    /// <summary>
    /// Utility function to retrieve a specific key from the Response Content
    /// </summary>
    /// <param name="key"></param>
    public object ContainsAtKey(string key)
    {
        if (ResponseContent.Contains(key))
        {
            return ResponseContent[key];
        }
        
        return null;
    }
    
    /// <summary>
    /// Try to retreieve a key from the ErrorsContent if it exists.
    /// </summary>
    public object ErrorsContentAtKey(string key)
    {
        if (ErrorsContent.Contains(key))
        {
            return ErrorsContent[key];
        }
        
        return null;
    }
}

public class TwitterPostTweetResponse : TwitterRestResponse {
    /// <summary>
    /// Overriding the default functionality here so that we ensure that the Response Content includes the
    /// keys "id_str" and "user".  If they are not present then there was some problem with the request.
    /// </summary>
    override public bool Success()
    {
        if (!base.Success())
        {
            return false;
        }
        
        return ContainsAtKey("id_str") != null && ContainsAtKey("user") != null;
    }
    
    public string GetTweetIdStr()
    {
        if (ContainsAtKey("id_str") != null)
        {
            return (string) ContainsAtKey("id_str");
        }
        
        return null;
    }
    
    public string GetScreenName()
    {
        if (ContainsAtKey("user") != null)
        {
            Dictionary<string, object> user = (Dictionary<string, object>) ContainsAtKey("user");
        	string screenName = (string) user["screen_name"];
            
            return screenName;
        }
        
        return null;
    }
}

public class TwitterUploadMediaResponse : TwitterRestResponse {
    protected TwitterUploadCommand UploadCommand = TwitterUploadCommand.UNCHUNKED;
    
    /// <summary>
    /// Overriding the default functionality here so that we can setup the UploadCommand property if the
    /// request that was made was part of a Chunked Media Upload.
    /// <summary>
    /// <param name="query"></param>
    /// <param name="request"></param>
    override public void Init(WWW query, WWWRequest request)
    {
        base.Init(query, request);
        
        // If the request we used had the "command" parameter set then it was part of a Chunked Media Upload
        string command = this._request.GetParameter("command");
        if (command != null)
        {
            UploadCommand = (TwitterUploadCommand) Enum.Parse(typeof(TwitterUploadCommand), command);
            
            switch(UploadCommand)
            {
                case TwitterUploadCommand.INIT:
            		SuccessStatus = HttpStatusCode.Accepted;
                    break;
                case TwitterUploadCommand.APPEND:
                    SuccessStatus = HttpStatusCode.NoContent;
                    break;
                case TwitterUploadCommand.FINALIZE:
                    SuccessStatus = HttpStatusCode.Created;
                    break;
                 default:
                    break;
            }   
        }
    }
    
    /// <summary>
    /// Overriding the default functionality here to ensure that the "media_id_string" exists on the Response
    /// content in some scenarios.  If that key is not present in those scenarios then the request was not
    /// actually successful.
    /// </summary>
    override public bool Success()
    {
        if (!base.Success())
        {
            return false;
        }
        
        switch(UploadCommand)
        {
            case TwitterUploadCommand.INIT:
                return ContainsAtKey("media_id_string") != null;
            case TwitterUploadCommand.APPEND:
                return true;
            case TwitterUploadCommand.FINALIZE:
                return ContainsAtKey("media_id_string") != null;
            case TwitterUploadCommand.UNCHUNKED:
                return ContainsAtKey("media_id_string") != null;
            default:
                return false;
        }
    }
    
    public string GetMediaIdString()
    {
        if (ContainsAtKey("media_id_string") != null)
        {
            return (string) ContainsAtKey("media_id_string");
        }
        
        return null;
    }
}
