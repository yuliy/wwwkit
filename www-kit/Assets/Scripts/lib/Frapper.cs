﻿using UnityEngine;

using System;
using System.Diagnostics;

namespace Frapper
{
    public class FFMPEG
    {
		#region Flags
   	    const string INPUT_FLAG = "-i";
		const string OVERWRITE_FLAG = "-y";
		const string AUDIO_BITRATE_FLAG = "-b:a";
		const string VIDEO_BITRATE_FLAG = "-b:v";
		const string VIDEO_CODEC_FLAG = "-vcodec";
		#endregion
		
		#region Codecs
		public const string CODEC_H264 = "h264";
		#endregion
		
		#region Properties
        private string _ffExe = "Assets/bin/ffmpeg";
        public string ffExe
        {
            get
            {
                return _ffExe;
            }
        }
        #endregion

        #region Constructors
        public FFMPEG(string ffmpegExePath)
        {
            _ffExe = ffmpegExePath;
        }
        #endregion
		
		#region Helpers
		private string BuildBitrate(int bitrate) {
			return bitrate.ToString() + "k";
		}
		
		public string BuildVideoConvertParameters(string inputPath, string outputPath, bool overwrite, string vcodec, int audioBitrate, int videoBitrate) {
			string parameters = "";
			
			parameters += FFMPEG.INPUT_FLAG + " " + inputPath + " ";
			if (overwrite) {
				parameters += FFMPEG.OVERWRITE_FLAG + " ";
			}
			parameters += FFMPEG.VIDEO_CODEC_FLAG + " " + vcodec + " ";
			parameters += FFMPEG.AUDIO_BITRATE_FLAG + " " + this.BuildBitrate(audioBitrate) + " ";
			parameters += FFMPEG.VIDEO_BITRATE_FLAG + " " + this.BuildBitrate(videoBitrate) + " ";
			parameters += outputPath;
			
			return parameters;
		}
		#endregion

        #region Run the process
		public void RunProcess(string Parameters, Action<bool> onComplete)
		{
			//create a process info
			var oInfo = new ProcessStartInfo(this._ffExe, Parameters)
			{
				UseShellExecute = false,
				CreateNoWindow = true,
				RedirectStandardOutput = true,
				RedirectStandardError = true
			};
			
			var output = string.Empty;
			
			try
			{  
				Process process = System.Diagnostics.Process.Start(oInfo);
				
				output = process.StandardError.ReadToEnd();
				process.WaitForExit();
                var exitStatus = (process.ExitCode == 0);
				process.Close();
                onComplete(exitStatus);
			}
			catch (Exception e)
			{
				output = string.Empty;
			}
		}
        #endregion
    }

}