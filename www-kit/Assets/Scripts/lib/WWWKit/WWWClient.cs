﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using WWWKit.Authenticators;

namespace WWWKit {
    public class WWWClient {
        private WWW _query;
        private WWWForm _form;
        
        public string BaseUrl { get; set; }
        
        public OAuth1Authenticator Authenticator { get; set; }
        
        /// <summary>
        /// Setup the BaseUrl property for the WWWClient.
        /// </summary>
        /// <param name="url"></param>
        public WWWClient(string url) {
            this.BaseUrl = url;
        }
        
        /// <summary>
        /// Utility function to convert a Dictionary<string, string> to a parameter string for insertion into a URL.  Credit to
        /// https://bitbucket.org/gambitforhire/twitter-search-with-unity as the originator of this functionality.
        /// </summary>
        /// <param name="paramsDictionary"></param>
        private string ParamDictionaryToString(Dictionary<string, string> paramsDictionary) {
    		StringBuilder dictionaryStringBuilder = new StringBuilder();       
    		foreach (KeyValuePair<string, string> keyValuePair in paramsDictionary)
            {
                //append a = between the key and the value and a & after the value
                dictionaryStringBuilder.Append(string.Format("{0}={1}&", keyValuePair.Key, keyValuePair.Value));
            }
    		
    		string paramString = dictionaryStringBuilder.ToString().Substring(0, dictionaryStringBuilder.Length - 1);
    		return paramString;
    	}
        
        /// <summary>
        /// Setup the internal WWW and/or WWWForm (if necessary) for use in an Execute or Execute_Multi call.
        /// </summary>
        /// <param name="request"></param>
        private void Setup(WWWRequest request) {
            string url = this.BaseUrl + request.Path;
            Dictionary<string, string> headers = new Dictionary<string, string>();
            byte[] data = null;
            
            if (request.Verb == HttpMethod.POST) {
                // Since this is a POST request we must setup an accompanying WWWForm object to assist with
                // the submission of the WWW.
                this._form = new WWWForm();
                
                // If we have no Request parameters, or we want to submit multipart/form-data content without
                // specifying any Binary data we must at least put one byte onto the body of the WWWForm object.
                // This is a known issue with Unity where it does not like trying to submit a form with an
                // empty body.
                if (request.Parameters.Count == 0) {
                    this._form.AddBinaryData("binary", new byte[1]);
                }
                
                if (request.AlwaysMultipart && request.BinaryData.Count == 0) {
                    this._form.AddBinaryData("binary", new byte[1]);
                }
                
                // move all Binary data from the WWWRequest to the WWWForm
                foreach(var b in request.BinaryData) {
                    this._form.AddBinaryData(b.Key, b.Value);
                }
                
                // move all Parameters from the WWWRequest to the WWWForm
                foreach(var p in request.Parameters) {
                    this._form.AddField(p.Key, p.Value);
                }
                
                // copy headers created on the WWWForm so we can use them to submit the WWW
                foreach (DictionaryEntry entry in this._form.headers)
        		{
        			headers [System.Convert.ToString (entry.Key)] = System.Convert.ToString (entry.Value);
        		}
                
                // retrieve the data from the WWWForm as a byte Array
                data = this._form.data;
            } else if (request.Verb == HttpMethod.GET) {
                if (request.Parameters.Count != 0) {
                    // At the moment Unity does not support GET requests with body parameters.  Some APIs however,
                    // work by just having these parameters in the URL string.
                    url += "?" + this.ParamDictionaryToString(request.Parameters);
                }
            } else {
                // For now we only support GET/POST, this is a known issue with Unity
                throw new Exception("Unsupported HTTP Method " + request.Verb);
            }
            
            // Use our Authenticator to build the Authorization header for the WWW request.  Note the usage of the
            // AlwaysMultipart parameter: if we are using this form to submit multipart/form-data content then
            // we must exclude all parameters except for those of name oauth_* (this is part of the oauth1 spec)
            string authHeader = Authenticator.BuildAuthenticationHeader(
                url, 
                request.Verb.ToString(), 
                !request.AlwaysMultipart ? request.Parameters : new Dictionary<string, string>()
            );
            headers["Authorization"] = authHeader;
            
            this._query = new WWW(url, data, headers);
        }
        
        /// <summary>
        /// Execute a given WWWRequest.  Use the onComplete Action once the requests finishes.  Expects to be used
        /// as part of a MonoBehaviour calling StartCoroutine.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="onComplete"></param>
        public IEnumerator Execute<T>(WWWRequest request, Action<T> onComplete)
        where T : WWWResponse, new()
        {
            this.Setup(request);
            
            yield return this._query;
            
            // build the WWWResponse object
            T response = new T();
            response.Init(this._query, request);
            
            onComplete(response);
        }
        
        /// <summary>
        /// Executes a given List of WWWRequest objects.  The onCompleteRequest Action will be called after each Request
        /// completes and the onCompleteMulti Action will be called at the end of all Requests complete.
        /// </summary>
        /// <param name="requests"></param>
        /// <param name="onCompleteRequest"></param>
        /// <param name="onCompleteMulti"></param>
        public IEnumerator Execute_Multi<T>(List<WWWRequest> requests, Action<T> onCompleteRequest, Action<T> onCompleteMulti)
        where T : WWWResponse, new()
        {
            foreach(WWWRequest request in requests) {
                this.Setup(request);
                yield return this._query;
                
                // build the WWWResponse object
                T response = new T();
                response.Init(this._query, request);
                onCompleteRequest(response);
            }
            
            // build the WWWResponse object
            T responseFinal = new T();
        	responseFinal.Init(this._query, requests[requests.Count - 1]);
            onCompleteMulti(responseFinal);
        }
    }
}
