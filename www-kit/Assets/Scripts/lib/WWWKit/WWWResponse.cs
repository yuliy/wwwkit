﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.Net;

using MiniJSON;

namespace WWWKit {
	public class WWWResponse {
		protected WWW _query;
        protected WWWRequest _request;
        
        /// <summary>
        /// Default constructor for the WWWResponse
        /// </summary>
        public WWWResponse() {  }
		
        /// <summary>
        /// Setup the WWWResponse by setting the _query and _request properties
        /// </summary>
        /// <param name="query"></param>
        /// <param name="request"></param>
		public WWWResponse(WWW query, WWWRequest request) {
			this._query = query;
            this._request = request;
		}
        
        /// <summary>
        /// Initialize a WWWResponse by setting the _query and _request properties
        /// </summary>
        /// <param name="query"></param>
        /// <param name="request"></param>
        virtual public void Init(WWW query, WWWRequest request) {
            this._query = query;
            this._request = request;
        }
        
        /// <summary>
        /// Determine if the WWW object was submitted successfully
        /// </summary>
        virtual public bool Success() {
            return this._query.error == null;
        }
		
        /// <summary>
        /// Retrieve the Errors on the WWW object
        /// </summary>
		virtual public string Errors() {
			return this._query.error;
		}
		
        /// <summary>
        /// Retrieve the Content on the WWW object
        /// </summary>
		public string Content() {
			return this._query.text;
		}
		
        /// <summary>
        /// Retrieve the Content on the WWW object as JSON
        /// </summary>
		public IDictionary ContentJson() {
			IDictionary content = (IDictionary) Json.Deserialize(this._query.text);
			return content;
		}
		
        /// <summary>
        /// Interpret the Content on the WWW object as a query string, return that information as
        /// a Dictionary<string, string>
        /// </summary>
		public Dictionary<string, string> QueryStringToDictionary() {
			var query = this._query.text;
			var dictionary = new Dictionary<string, string>();
			
			var sep = new char[1];
			sep[0] = '&';
			var parameters = query.Split(sep);
			
			foreach(var pair in parameters) {
				var sepEq = new char[1];
				sepEq[0] = '=';
				
				var pairSplit = pair.Split(sepEq);
				if (pairSplit.Length == 2) {
					var key = pairSplit[0];
					var val = pairSplit[1];
					
					dictionary[key] = val;
				}
			}
			
			return dictionary;
		}
		
        /// <summary>
        /// Convert the STATUS Response Header into an HttpStatusCode if possible.  Returns HttpStatusCode.Ambiguous
        /// if the STATUS Header cannot be interpreted.
        /// </summary>
		public HttpStatusCode StatusCode() {
			string status = this.Header("STATUS");
			HttpStatusCode code;
			switch(status) {
                case "HTTP/1.1 100 Continue":
                    code = HttpStatusCode.Continue;
                    break;
				case "200 OK":
					code = HttpStatusCode.OK;
					break;
				case "201 Created":
					code = HttpStatusCode.Created;
					break;
                case "202 Accepted":
                    code = HttpStatusCode.Accepted;
                    break;
				case "204 No Content":
					code = HttpStatusCode.NoContent;
					break;
				case "HTTP/1.1 401 Authorization Required":
					code = HttpStatusCode.Unauthorized;
					break;
                case "403 Forbidden":
                    code = HttpStatusCode.Forbidden;
                    break;
                case "404 Not Found":
                    code = HttpStatusCode.NotFound;
                    break;
				default:
					code = HttpStatusCode.Ambiguous;
					break;
			}
			
			return code;
		}
		
        /// <summary>
        /// Retrieve a given header from the WWW response headers if it exists
        /// </summary>
        /// <param name="key"></param>
		public string Header(string key) {
			key = key.ToUpper();
			if (this._query.responseHeaders.ContainsKey(key)) {
				return this._query.responseHeaders[key];
			}
			
			return null;
		}
        
        /// <summary>
        /// Retrieve all WWW response headers
        /// </summary>
        public Dictionary<string, string> Headers() {
            return this._query.responseHeaders;
        }
	}
}
