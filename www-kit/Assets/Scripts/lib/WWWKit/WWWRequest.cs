﻿using UnityEngine;
using System.Collections.Generic;

namespace WWWKit {
    
    /// <summary>
    /// Types of all the possible HTTP Request Methods
    /// </summary>
    public enum HttpMethod {
        GET,
        POST
    }
    
    public class WWWRequest {
        public string Path { get; set; }
        public HttpMethod Verb { get; set; }
        public bool AlwaysMultipart = false;
        
        public Dictionary<string, string> Parameters { get; set; }
        public Dictionary<string, byte[]> BinaryData { get; set; }
        
        /// <summary>
        /// Setup the Path and Verb on the WWWRequest, initialize the Parameters and BinaryData
        /// Lists.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="verb"></param>
        public WWWRequest(string path, HttpMethod verb) {
            this.Path = path;
            this.Verb = verb;
            
            this.Parameters = new Dictionary<string, string>();
            this.BinaryData = new Dictionary<string, byte[]>();
        }
        
        /// <summary>
        /// Add a Parameter to the Request Parameters
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddParameter(string key, string value) {
            this.Parameters.Add(key, value);
        }
        
        /// <summary>
        /// Retrieve a Parameter from the Request Parameters if it exists
        /// </summary>
        /// <param name="key"></param>
        public string GetParameter(string key) {
            if (this.Parameters.ContainsKey(key)) {
                return this.Parameters[key];
            }
            
            return null;
        }
        
        /// <summary>
        /// Add some Binary Data to the Request Binary Data
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        public void AddBinaryData(string key, byte[] data) {
            this.BinaryData.Add(key, data);
        }
    }
}
