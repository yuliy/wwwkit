﻿using UnityEngine;

using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

using System.Linq;
using System.Text.RegularExpressions;

namespace WWWKit.Authenticators {
    /// <summary>
    /// Types of OAuth Authentication requirements
    /// </summary>
    public enum OAuthType {
        RequestToken,
        AccessToken,
        ProtectedResource
    }
    
	public class OAuth1Authenticator {
		// The below help methods are modified from "WebRequestBuilder.cs" in Twitterizer(http://www.twitterizer.net/).
		// Here is its license.
		
		//-----------------------------------------------------------------------
		// <copyright file="WebRequestBuilder.cs" company="Patrick 'Ricky' Smith">
		//  This file is part of the Twitterizer library (http://www.twitterizer.net/)
		// 
		//  Copyright (c) 2010, Patrick "Ricky" Smith (ricky@digitally-born.com)
		//  All rights reserved.
		//  
		//  Redistribution and use in source and binary forms, with or without modification, are 
		//  permitted provided that the following conditions are met:
		// 
		//  - Redistributions of source code must retain the above copyright notice, this list 
		//    of conditions and the following disclaimer.
		//  - Redistributions in binary form must reproduce the above copyright notice, this list 
		//    of conditions and the following disclaimer in the documentation and/or other 
		//    materials provided with the distribution.
		//  - Neither the name of the Twitterizer nor the names of its contributors may be 
		//    used to endorse or promote products derived from this software without specific 
		//    prior written permission.
		// 
		//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
		//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
		//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
		//  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
		//  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
		//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
		//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
		//  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
		//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
		//  POSSIBILITY OF SUCH DAMAGE.
		// </copyright>
		// <author>Ricky Smith</author>
		// <summary>Provides the means of preparing and executing Anonymous and OAuth signed web requests.</summary>
		//-----------------------------------------------------------------------
		
		private static readonly string[] OAuthParametersToIncludeInHeader = new[]
		{
			"oauth_version",
			"oauth_nonce",
			"oauth_timestamp",
			"oauth_signature_method",
			"oauth_consumer_key",
			"oauth_token",
			"oauth_verifier"
			// Leave signature omitted from the list, it is added manually
			// "oauth_signature",
		};
		
		private static readonly string[] SecretParameters = new[]
		{
			"oauth_consumer_secret",
			"oauth_token_secret",
			"oauth_signature"
		};
        
        internal string ConsumerKey { get; set; }
        internal string ConsumerSecret { get; set; }
        internal string Token { get; set; }
        internal string TokenSecret { get; set; }
        internal string Verifier { get; set; }
        internal OAuthType Type { get; set; }
        
        # region Helpers
        /// <summary>
        /// Utility function to generate an oauth_timestamp
        /// </summary>
		private static string GenerateTimeStamp()
		{
			// Default implementation of UNIX time of the current UTC time
			TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
			return Convert.ToInt64(ts.TotalSeconds, CultureInfo.CurrentCulture).ToString(CultureInfo.CurrentCulture);
		}
		
        /// <summary>
        /// Utility function to generate an oauth_nonce
        /// </summary>
		private static string GenerateNonce()
		{
			// Just a simple implementation of a random number between 123400 and 9999999
			return new System.Random().Next(123400, int.MaxValue).ToString("X", CultureInfo.InvariantCulture);
		}
		
        /// <summary>
        /// Utility function to Normalize a given Uri
        /// </summary>
        /// <param name="url"></param
		private static string NormalizeUrl(Uri url)
		{
			string normalizedUrl = string.Format(CultureInfo.InvariantCulture, "{0}://{1}", url.Scheme, url.Host);
			if (!((url.Scheme == "http" && url.Port == 80) || (url.Scheme == "https" && url.Port == 443)))
			{
				normalizedUrl += ":" + url.Port;
			}
			
			normalizedUrl += url.AbsolutePath;
			return normalizedUrl;
		}
		
        /// <summary>
        /// Utility function to Url Encode a string
        /// </summary>
        /// <param name="value"></param
		private static string UrlEncode(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return string.Empty;
			}
			
			value = Uri.EscapeDataString(value);
			
			// UrlEncode escapes with lowercase characters (e.g. %2f) but oAuth needs %2F
			value = Regex.Replace(value, "(%[0-9a-f][0-9a-f])", c => c.Value.ToUpper());
			
			// these characters are not escaped by UrlEncode() but needed to be escaped
			value = value
				.Replace("(", "%28")
					.Replace(")", "%29")
					.Replace("$", "%24")
					.Replace("!", "%21")
					.Replace("*", "%2A")
					.Replace("'", "%27");
			
			// these characters are escaped by UrlEncode() but will fail if unescaped!
			value = value.Replace("%7E", "~");
			
			return value;
		}
		
        /// <summary>
        /// Utility function to Url Encode an IEnumberable<KeyValuePair<string, string>> of parameters
        /// </summary>
        /// <param name="parameters"></params>
		private static string UrlEncode(IEnumerable<KeyValuePair<string, string>> parameters)
		{
			StringBuilder parameterString = new StringBuilder();
			
			var paramsSorted = from p in parameters
				orderby p.Key, p.Value
					select p;
			
			foreach (var item in paramsSorted)
			{
				if (parameterString.Length > 0)
				{
					parameterString.Append("&");
				}
				
				parameterString.Append(
					string.Format(
					CultureInfo.InvariantCulture,
					"{0}={1}",
					UrlEncode(item.Key),
					UrlEncode(item.Value)));
			}
			
			return UrlEncode(parameterString.ToString());
		}
        
        /// <summary>
        /// Generate the oauth_signature
        /// </summary>
        /// <param name="httpMethod"></param>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
		private static string GenerateSignature(string httpMethod, string url, Dictionary<string, string> parameters)
		{
			var nonSecretParameters = (from p in parameters
			                           where !SecretParameters.Contains(p.Key)
			                           select p);
			
			// Create the base string. This is the string that will be hashed for the signature.
			string signatureBaseString = string.Format(CultureInfo.InvariantCulture,
			                                           "{0}&{1}&{2}",
			                                           httpMethod,
			                                           UrlEncode(NormalizeUrl(new Uri(url))),
			                                           UrlEncode(nonSecretParameters));
			
			// Create our hash key (you might say this is a password)
			string key = string.Format(CultureInfo.InvariantCulture,
			                           "{0}&{1}",
			                           UrlEncode(parameters["oauth_consumer_secret"]),
			                           parameters.ContainsKey("oauth_token_secret") ? UrlEncode(parameters["oauth_token_secret"]) : string.Empty);
			
			
			// Generate the hash
			HMACSHA1 hmacsha1 = new HMACSHA1(Encoding.ASCII.GetBytes(key));
			byte[] signatureBytes = hmacsha1.ComputeHash(Encoding.ASCII.GetBytes(signatureBaseString));
			return Convert.ToBase64String(signatureBytes);
		}
		#endregion
        
        /// <summary>
        /// Add the Default OAuth parameters to the parameters used to generate the OAuth Authorization header
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
		private static void AddDefaultOAuthParams(Dictionary<string, string> parameters, string consumerKey, string consumerSecret)
		{
			parameters.Add("oauth_version", "1.0");
			parameters.Add("oauth_nonce", GenerateNonce());
			parameters.Add("oauth_timestamp", GenerateTimeStamp());
			parameters.Add("oauth_signature_method", "HMAC-SHA1");
			parameters.Add("oauth_consumer_key", consumerKey);
			parameters.Add("oauth_consumer_secret", consumerSecret);
		}
        
        /// <summary>
        /// Retrieve an OAuth1Authenticator configured for retrieving a Request Token
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        public static OAuth1Authenticator ForRequestToken(string consumerKey, string consumerSecret)
        {
            OAuth1Authenticator authenticator = new OAuth1Authenticator
            {
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                Type = OAuthType.RequestToken
            };
            
            return authenticator;
        }
		
        /// <summary>
        /// Retrieve the OAuth Authorization header for retrieving a Request Token
        /// </summary>
        /// <param name="httpRequestType"></param>
        /// <param name="apiUrl"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <param name="parameters"></param>
		public static string GetHeaderForRequestToken(string httpRequestType, string apiUrl, string consumerKey, string consumerSecret, Dictionary<string, string> parameters)
		{
			AddDefaultOAuthParams(parameters, consumerKey, consumerSecret);
			
			return GetFinalOAuthHeader(httpRequestType, apiUrl, parameters);
		}
        
        /// <summary>
        /// Retrieve an OAuth1Authenticator configured for retrieving an Access Token
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <param name="requestToken"></param>
        /// <param name="verifier"></param>
        public static OAuth1Authenticator ForAccessToken(string consumerKey, string consumerSecret, string requestToken, string verifier) {
            OAuth1Authenticator authenticator = new OAuth1Authenticator
            {
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                Token = requestToken,
                Verifier = verifier,
                Type = OAuthType.AccessToken
            };
            
            return authenticator;
        }
		
        /// <summary>
        /// Retrieve the OAuth Authorization header for retrieving an Access Token
        /// </summary>
        /// <param name="httpRequestType"></param>
        /// <param name="apiUrl"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <param name="requestToken"></param>
        /// <param name="verifier"></param>
        /// <param name="parameters"></param>
		public static string GetHeaderForAccessToken(string httpRequestType, string apiUrl, string consumerKey, string consumerSecret, string requestToken, string verifier, Dictionary<string, string> parameters)
        {			
			AddDefaultOAuthParams(parameters, consumerKey, consumerSecret);
			parameters.Add("oauth_token", requestToken);
			parameters.Add("oauth_verifier", verifier);
			
			return GetFinalOAuthHeader(httpRequestType, apiUrl, parameters);
		}
        
        /// <summary>
        /// Retrieve an OAuth1Authenticator configured for accessing a Protected Resource
        /// </summary>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <param name="token"></param>
        /// <param name="tokenSecret"></param>
        public static OAuth1Authenticator ForProtectedResource(string consumerKey, string consumerSecret, string token, string tokenSecret)
        {
            OAuth1Authenticator authenticator = new OAuth1Authenticator
            {
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                Token = token,
                TokenSecret = tokenSecret,
                Type = OAuthType.ProtectedResource
            };
            
            return authenticator;
        }
		
        /// <summary>
        /// Retrieve the OAuth Authorization header for accessing a Protected Resource
        /// </summary>
        /// <param name="httpRequestType"></param>
        /// <param name="apiURL"></param>
        /// <param name="consumerKey"></param>
        /// <param name="consumerSecret"></param>
        /// <param name="token"></param>
        /// <param name="tokenSecret"></param>
        /// <param name="parameters"></param>
		public static string GetHeaderWithAccessToken(string httpRequestType, string apiURL, string consumerKey, string consumerSecret, string token, string tokenSecret, Dictionary<string, string> parameters)
		{
			AddDefaultOAuthParams(parameters, consumerKey, consumerSecret);
			
			parameters.Add("oauth_token", token);
			parameters.Add("oauth_token_secret", tokenSecret);
			
			return GetFinalOAuthHeader(httpRequestType, apiURL, parameters);
		}
		
        /// <summary>
        /// Retrieve the OAuth Authorization Header based on the configured OAuth1Authenticator
        /// </summary>
        /// <param name="HTTPRequestType"></param>
        /// <param name="URL"></param>
        /// <param name="parameters"></param>
		private static string GetFinalOAuthHeader(string HTTPRequestType, string URL, Dictionary<string, string> parameters)
		{
			// Add the signature to the oauth parameters
			string signature = GenerateSignature(HTTPRequestType, URL, parameters);
			
			parameters.Add("oauth_signature", signature);
			
			StringBuilder authHeaderBuilder = new StringBuilder();
			authHeaderBuilder.AppendFormat("OAuth realm=\"{0}\"", "Twitter API");
			
			var sortedParameters = from p in parameters
				where OAuthParametersToIncludeInHeader.Contains(p.Key)
					orderby p.Key, UrlEncode(p.Value)
					select p;
			
			foreach (var item in sortedParameters)
			{
				authHeaderBuilder.AppendFormat(",{0}=\"{1}\"", UrlEncode(item.Key), UrlEncode(item.Value));
			}
			
			authHeaderBuilder.AppendFormat(",oauth_signature=\"{0}\"", UrlEncode(parameters["oauth_signature"]));
			
			return authHeaderBuilder.ToString();
		}
        
        /// <summary>
        /// Build the Authorization header based on our configured OAuth1Authenticator
        /// </summary>
        /// <param name="url"></param>
        /// <param name="httpMethod"></param>
        /// <param name="parameters"></param>
        public string BuildAuthenticationHeader(string url, string httpMethod, Dictionary<string, string> parameters = null)
        {
            // call the appropriate Header builder based on our OAuth type
            string authHeader = null;
            switch(Type) {
                case OAuthType.RequestToken:
                    authHeader = OAuth1Authenticator.GetHeaderForRequestToken(
                        httpMethod,
                        url,
                        this.ConsumerKey,
                        this.ConsumerSecret,
                        new Dictionary<string, string>()
                    );
                    break;
                case OAuthType.AccessToken:
                    authHeader = OAuth1Authenticator.GetHeaderForAccessToken(
                        httpMethod,
                        url,
                        this.ConsumerKey,
                        this.ConsumerSecret,
                        this.Token,
                        this.Verifier,
                        new Dictionary<string, string>()
                    );
                    break;
                case OAuthType.ProtectedResource:
                    authHeader = OAuth1Authenticator.GetHeaderWithAccessToken(
                        httpMethod,
                        url,
                        this.ConsumerKey,
                        this.ConsumerSecret,
                        this.Token,
                        this.TokenSecret,
                        parameters
                    );
                    break;
                default:
                    // maybe throw an exception here? (unsupported OAuth type)
                    break;
            }
            
            return authHeader;
        }
	}

}
